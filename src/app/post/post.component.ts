import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent {
  @Input() title: string;
  @Input() link: string;
  @Input() summary: string;
  @Input() imgUrl: string;
  @Input() sourceName: string;
  @Input() sourceUrl: string;

  constructor() { }
}
