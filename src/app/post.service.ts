import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {Observable, of} from 'rxjs/index';
import {catchError, map, tap} from 'rxjs/internal/operators';
import {MessageService} from './message.service';
import {ResponsePosts} from './models';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({providedIn: 'root'})
export class PostService {

  constructor(private http: HttpClient, private messageSrv: MessageService,
              @Inject('apiBaseUrl') private apiBaseUrl: string) {
  }

  public getPosts(params: Object): Observable<Object> {
    return this.http.get<Object>(this.apiBaseUrl + 'posts', { params })
      .pipe(
        tap(posts => this.log(`fetched posts`)),
        catchError(this.handleError('getPosts', []))
      );
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    this.messageSrv.add('HeroService: ' + message);
  }

}
