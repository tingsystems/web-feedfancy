
export class ResponsePosts {
  count: Number;
  next: string;
  previus: string;
  results: Array<Post>;
}

export class Taxonomy {
  slug: string;
  name: string;
}

export class Source {
  url: string;
  name: string;
}

export class Post {
  title: string;
  content: string;
  summary: string;
  link: string;
  imgUrl: string;
  slug: string;
  source: Source;
  taxonomies: Taxonomy[];
}
